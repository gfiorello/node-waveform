# node-waveform

![](http://i.imgur.com/oNy41Cr.png)

Input: any format audio or video file

Output:
  * waveform.js-compatible JSON representation of the audio file

This is a [Node.js](http://nodejs.org) module that wraps the waveform command
line interface. The C code is bundled
from that repository, so if you want to file an issue with or fork the C code,
go to the waveform repository.

## Usage

```js
var waveform = require('waveform');

waveform(audiofile, {
  // options
  'scan' //whether to do a pass to detect duration

  output: "outputfile.json",  // path to output-file, or - for stdout as a Buffer
  'width': 800,               // width in samples
  'precision': 4,             // how many digits of precision
  'metadata': false,             // include metadata in output JSON (default off)

}, function(err, buf) {
  // done
});
```

## Installation

1. Install [libgroove](https://github.com/andrewrk/libgroove) dev package.
   Only the main library is needed. Packages are available for common
   package managers.

2. Install libpng and zlib dev packages.

3. `npm install waveform`
